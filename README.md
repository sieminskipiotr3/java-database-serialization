This application provides simplified person's database storage.

It reads and validates persons' list from a text file. 

It allows sorting on chosen specifics.

It allows to serialize and deserialize the database afterwards.
