package psieminski;

import java.io.IOException;
import java.io.Serial;

public class Assignment08Exception extends Exception {

    @Serial
    private static final long serialVersionUID = -6901018906165154875L;
    private final Exception IOException;

    public Assignment08Exception(String message, Throwable cause) {
        super(message, cause);
        this.IOException = new IOException();
    }

    public Exception getException() {
        return IOException;
    }

}
