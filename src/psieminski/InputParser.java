package psieminski;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;
import java.util.regex.Pattern;

public final class InputParser {

    private static final DateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
    private static final String pattern = "\\w+\\s\\w+\\s[0-2][0-9]{3}\\-[0-2][0-9]\\-[0-3][0-9]";
    private static final Pattern regex = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);

    public static List<Person> parse(File file) throws IOException {

        if (!file.exists()) {
            throw new InputMismatchException("File path provided does not contain the input file");
        }

        List<Person> listOfPersons = new ArrayList<>();
        try (BufferedReader bReader = new BufferedReader(new FileReader(file))) {

            String line;

            while ((line = bReader.readLine()) != null) {

                if(!regex.matcher(line).matches()) {
                    continue;
                }

                String[] arr = line.split(" ");
                String name = arr[0];
                String lastName = arr[1];
                Date birthDate;
                try {
                    birthDate = simpleDate.parse(arr[2]);
                } catch (ParseException exception) {
                    continue;
                }
                listOfPersons.add(new Person(name, lastName, birthDate));

            }
        } catch (IOException exception) {
            throw new IOException("Lack of information to read from the file specified. Check the format.");
        }
        return listOfPersons;
    }

}
