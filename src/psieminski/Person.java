package psieminski;

import java.io.*;
import java.util.Date;

//TODO validation 1 of 4 requirements
public final class Person implements Comparable<Person> {

    private final String _firstName;
    private final String _surname;
    private final Date _birthdate;

    public Person(String firstName, String surname, Date birthdate) {
        _firstName = firstName;
        _surname = surname;
        _birthdate = birthdate;
    }

    public String get_firstName() {
        return _firstName;
    }

    public String get_surname() {
        return _surname;
    }

    public Date get_birthdate() {
        return _birthdate;
    }

    @Override
    public String toString() {
        return _firstName + " " + _surname + " " + _birthdate.getTime();
    }

    @Override
    public int compareTo(Person otherPerson) {
        // natural order based on:
        // (1) surname;
        int value = this.get_surname().compareToIgnoreCase(otherPerson.get_surname());
        if (value != 0) {
            return value;
        }
        // (2) first name;
        value = this.get_firstName().compareToIgnoreCase(otherPerson.get_firstName());
        if (value != 0) {
            return value;
        }
        // (3) birth date.
        value = this.get_birthdate().compareTo(otherPerson.get_birthdate());
        return value;
    }

    public void serialize(DataOutputStream output) throws Exception {

        try {
            output.writeUTF(this.toString());
            output.writeChars("\r\n");
        } catch (IOException exception) {
            throw new Assignment08Exception("Exception: ", exception.getCause()).getException();
        }
    }

    public static Person deserialize(DataInputStream input) throws Exception {

        String firstName;
        String lastName;
        Date birthDate;

        try {

            String[] s = input.readUTF().split(" ");
            firstName = s[0];
            lastName = s[1];
            birthDate = new Date(Long.parseLong(s[2]));

        } catch (IOException e) {
            throw new Assignment08Exception("Exception: ", e.getCause()).getException();
        }

        return new Person(firstName, lastName, birthDate);
    }

}
