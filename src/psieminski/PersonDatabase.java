package psieminski;

import psieminski.comparators.BirthdateComparator;
import psieminski.comparators.FirstNameComparator;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public final class PersonDatabase {

    private static final Comparator<Person> naturalOrder = Comparator.naturalOrder();
    private static final Comparator<Person> byName = new FirstNameComparator();
    private static final Comparator<Person> byBirthDate = new BirthdateComparator();
    private final List<Person> naturalOrderList;
    private final List<Person> byNameList;
    private final List<Person> byDateList;
    private final Map<Date, List<Person>> byDateSearchCollection;

    public PersonDatabase(List<Person> listOfPersons) {

        naturalOrderList = new ArrayList<>(listOfPersons);
        naturalOrderList.sort(naturalOrder);

        byNameList = new ArrayList<>(listOfPersons);
        byNameList.sort(byName);

        byDateList = new ArrayList<>(listOfPersons);
        byDateList.sort(byBirthDate);

        byDateSearchCollection = listOfPersons
                .stream()
                .collect(Collectors.groupingBy(Person::get_birthdate, TreeMap::new, Collectors.mapping(x -> x, Collectors.toList())));
    }

    public PersonDatabase(File file) throws IOException {
        this(InputParser.parse(file));
    }

    public List<Person> sortedByFirstName() {
        return byNameList;
    }

    public List<Person> sortedBySurnameFirstNameAndBirthdate() {
        return naturalOrderList; // natural order (Comparable)
    }

    public List<Person> sortedByBirthdate() {
        return byDateList; // external rule for ordering (based on Comparator --- BirthdateComparator)
    }

    public List<Person> bornOnDay(Date date) {
        return byDateSearchCollection.get(date);
    }

    public void serialize(DataOutputStream output) {
        sortedByFirstName()
                .forEach(p -> {

                    try {
                        output.writeUTF(p.toString());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                });
    }

    public static PersonDatabase deserialize(DataInputStream input) throws IOException {

        String firstName;
        String lastName;
        Date birthDate;
        List<Person> personList = new ArrayList<>();
        boolean eof = false;

        while (!eof) {
            try {
                String[] s = input.readUTF().split(" ");
                firstName = s[0];
                lastName = s[1];
                birthDate = new Date(Long.parseLong(s[2]));
                Person p = new Person(firstName, lastName, birthDate);
                personList.add(p);
            } catch (EOFException e) {
                eof = true;
            }
        }

        return new PersonDatabase(personList);
    }

}
