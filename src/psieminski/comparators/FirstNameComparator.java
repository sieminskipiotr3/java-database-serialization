package psieminski.comparators;

import psieminski.Person;

import java.util.Comparator;

public class FirstNameComparator implements Comparator<Person> {

    @Override
    public int compare(Person person1, Person person2) {
        return person1.get_firstName().compareToIgnoreCase(person2.get_firstName());
    }
}

