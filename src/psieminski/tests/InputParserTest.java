package psieminski.tests;

import org.junit.jupiter.api.BeforeEach;
import psieminski.InputParser;
import psieminski.Person;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.InputMismatchException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class InputParserTest {

    private static final File file = new File("src/psieminski/inputList.txt");
    private static final DateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
    List<Person> listOfPersons;

    @BeforeEach
    void checkForFile() {
        assertTrue(file.exists());
    }

    @org.junit.jupiter.api.Test
    void checkNotNull() throws IOException {
        assertNotNull(InputParser.parse(file));
    }

    @org.junit.jupiter.api.Test
    void inputMismatchErrorCheck() {
        File check = new File("src/psieminski/hereIsNoFileWithThisName");
        Exception exception = assertThrows(InputMismatchException.class, () -> InputParser.parse(check));
        assertEquals("File path provided does not contain the input file", exception.getMessage());
    }

    @org.junit.jupiter.api.Test
    void parse() throws ParseException, IOException {
        listOfPersons = InputParser.parse(file);
        assertEquals("Putin", listOfPersons.get(7).get_surname());
        assertEquals("Pacho", listOfPersons.get(13).get_firstName());
        assertEquals(new Date(String.valueOf(simpleDate.parse("1964-03-16"))), listOfPersons.get(9).get_birthdate());
    }
}
