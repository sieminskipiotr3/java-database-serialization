package psieminski.tests;

import org.junit.After;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import psieminski.Person;
import psieminski.PersonDatabase;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class PersonDatabaseTest {

    private static final File file = new File("src/psieminski/inputList.txt");
    private static final DateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
    FileOutputStream fileOut = new FileOutputStream("database.dat");
    DataOutputStream dout = new DataOutputStream(fileOut);
    FileInputStream fileIn = new FileInputStream("database.dat");
    DataInputStream din = new DataInputStream(fileIn);
    PersonDatabase dataBase;

    PersonDatabaseTest() throws FileNotFoundException {
    }

    @BeforeEach
    void createDataBase() throws IOException {
        dataBase = new PersonDatabase(file);
    }

    @After
    public void closeStreams() throws IOException {
        dout.close();
        din.close();
    }

    @Test
    void sortedByFirstName() {
        assertEquals("Abdul", dataBase.sortedByFirstName().get(0).get_firstName());
    }

    @Test
    void sortedBySurnameFirstNameAndBirthdate() {
        assertEquals("Ahmed", dataBase.sortedBySurnameFirstNameAndBirthdate().get(0).get_firstName());
    }

    @Test
    void sortedByBirthdate() {
        assertEquals("Petrochenkov", dataBase.sortedByBirthdate().get(13).get_surname());
    }

    @Test
    void checkForInputNotMatchingRegex() {
        Exception exception = assertThrows(IndexOutOfBoundsException.class, () -> dataBase.sortedByBirthdate().get(14));
        assertEquals("Index 14 out of bounds for length 14", exception.getMessage());
    }

    @Test
    void bornOnDay() throws ParseException {
        Date input = new Date(String.valueOf(simpleDate.parse("2000-02-15")));
        List<Person> expected = new ArrayList<>();
        Person first = new Person("Vera", "Verahow", input);
        Person second = new Person("Abdul", "Abudl", input);
        Person third = new Person("Theodor", "Strelecky", input);
        expected.add(first);
        expected.add(second);
        expected.add(third);
        assertEquals(expected.get(1).get_firstName(), dataBase.bornOnDay(input).get(1).get_firstName());
        assertEquals(expected.get(2).get_surname(), dataBase.bornOnDay(input).get(2).get_surname());
    }

    @Test
    void serialize() {
        dataBase.serialize(dout);
    }

    @Test
    void deserialize() throws IOException {

        dataBase.serialize(dout);
        PersonDatabase testDatabase = PersonDatabase.deserialize(din);
        List<Person> naturalOrderList = testDatabase.sortedByFirstName();

        String index0Name = naturalOrderList.get(0).get_firstName();
        String index5Surname = naturalOrderList.get(5).get_surname();
        Date index10Date = naturalOrderList.get(10).get_birthdate();

        String expectedName = dataBase.sortedByFirstName().get(0).get_firstName();
        String expectedSurname = dataBase.sortedByFirstName().get(5).get_surname();
        Date expectedDate = dataBase.sortedByFirstName().get(10).get_birthdate();

        assertEquals(expectedName, index0Name);
        assertEquals(index5Surname, expectedSurname);
        assertEquals(index10Date, expectedDate);
    }
}

