package psieminski.tests;

import org.junit.After;
import org.junit.jupiter.api.Test;
import psieminski.Person;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

class PersonTest {

    FileOutputStream fileOut = new FileOutputStream("output.dat");
    DataOutputStream dout = new DataOutputStream(fileOut);
    private static final DateFormat simpleDate = new SimpleDateFormat("yyyy-MM-dd");
    FileInputStream fileIn = new FileInputStream("output.dat");
    DataInputStream din = new DataInputStream(fileIn);
    Person person = new Person("Test", "testing", simpleDate.parse("1950-04-12"));

    PersonTest() throws FileNotFoundException, ParseException {
    }

    @After
    public void closeStreams() throws IOException {
        dout.close();
        din.close();
    }

    @Test
    void serialize() throws Exception {
        person.serialize(dout);
    }

    @Test
    void deserialize() throws Exception {

        person.serialize(dout);

        String nameCompare = "Test";
        String lastNameCompare = "testing";
        Date dateCompare = simpleDate.parse("1950-04-12");

        var result = Person.deserialize(din);

        String deserializedName = result.get_firstName();
        String deserializedLastName = result.get_surname();
        Date deserializedDate = result.get_birthdate();

        assertEquals(nameCompare, deserializedName);
        assertEquals(lastNameCompare, deserializedLastName);
        assertEquals(dateCompare, deserializedDate);

    }
}

